# ToDoList

This is a to do list assessment. The application is about adding, reading, updating and deleting to do items from the to do list.

**Assessment answers**
1. Test Plan: https://gitlab.com/Andisiwe/todolist/-/tree/master/Assessment/TestPlan
2. Bugs: https://gitlab.com/Andisiwe/todolist/-/tree/master/Assessment/Bugs
3. Automated tests: https://gitlab.com/Andisiwe/todolist/

**To execute the automated tests you need the following:**
- Visual studio 
- C#
- NUnit
- Selenium webdriver nuget package
- Selenium chromedriver nuget package
