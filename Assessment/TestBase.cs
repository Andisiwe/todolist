﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assessment
{
    public class TestBase
    {
        public static IWebDriver driver;
        static string filePath = AppDomain.CurrentDomain.BaseDirectory;
        static int bin = filePath.LastIndexOf("bin");
        public string projectDirectory = filePath.Remove(bin);

        [SetUp]
        public void BeforeTest()
        {
            driver = new ChromeDriver(projectDirectory + "\\Webdriver");
            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void AfterTest()
        {
            driver.Close();
            driver.Quit();
        }
    }
}
