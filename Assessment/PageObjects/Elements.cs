﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assessment.PageObjects
{
    public class Elements
    {
        public static By AddItemTextField => By.XPath("//input[@name='newtodo']");
        public static By AddButton => By.XPath("//input[@id='new-submit']");
        public static By UpdateTextField => By.XPath("//input[@name='edittodo']");
        public static By UpdateButton => By.XPath("//input[@id='edit-submit-0']");
        public static By DeleteButton => By.XPath("//form[@class='edit-todo-form']/a[@href='/todo/delete/0']");
    }
}
