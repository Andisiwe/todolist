﻿using Assessment.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Assessment.Test
{
    public class ToDoList
    {
        public void AddItem(IWebDriver driver, string item)
        {
            driver.FindElement(Elements.AddItemTextField).SendKeys(item);
            Console.WriteLine("Text added to the add item text field");
            driver.FindElement(Elements.AddButton).Click();
            Console.WriteLine("Add button clicked");

            driver.Navigate().Refresh();

            Assert.IsTrue(driver.FindElement(By.XPath("//span[contains(@id,'span-todo')][text()='" + item + "']")).Displayed);
        }

        public void UpdateItem(IWebDriver driver, string item)
        {
            driver.FindElement(Elements.UpdateTextField).SendKeys(item);
            Console.WriteLine("Text added to the update item text field");
            driver.FindElement(Elements.UpdateButton).Click();
            Console.WriteLine("Update button clicked");
        }

        public void DeleteItem(IWebDriver driver, string item)
        {
            Thread.Sleep(2000);
            driver.FindElement(Elements.DeleteButton).Click();
            Console.WriteLine("Delete button clicked");

            try
            {
                Assert.IsFalse(driver.FindElement(By.XPath("//span[contains(@id,'span-todo')][text()='" + item + "']")).Displayed);
            }
            catch (Exception e)
            {
                Console.WriteLine("To do item deleted");
            }
        }
    }
}
