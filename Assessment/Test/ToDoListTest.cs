﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assessment.Test
{
    [TestFixture("http://localhost:8080", "To do Item")]
    class ToDoListTest : TestBase
    {
        string url;
        string item;
        ToDoList toDoList = new ToDoList();
        
        public ToDoListTest(string url, string item)
        {
            this.url = url;
            this.item = item;
        }

        [Test, Order(1)]
        public void AddItem()
        {
            driver.Navigate().GoToUrl(url);
            toDoList.AddItem(driver, item);
        }

        [Test, Order(2)]
        public void UpdateItem()
        {
            driver.Navigate().GoToUrl(url);
            toDoList.UpdateItem(driver, item);
        }

        [Test, Order(3)]
        public void DeleteItem()
        {
            driver.Navigate().GoToUrl(url);
            toDoList.DeleteItem(driver, item);
        }
    }
}
